import React from 'react';
import './App.css';
import CoinList from './components/CoinList';
import CoinInfo from './components/CoinInfo';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <CoinList/>
        <CoinInfo/>
      </header>
    </div>
  );
}

export default App;
