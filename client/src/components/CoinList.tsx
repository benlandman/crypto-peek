import React from "react";
import { Button, Heading, Box, Spacer, Wrap } from "@chakra-ui/react";
import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  useQuery,
  gql,
} from "@apollo/client";

const CoinList = () => {
  const COINS = gql`
    query GetCoins {
      coins {
        id
        symbol
        name
      }
    }
  `;

  const DisplayCoins = () => {
    const { loading, error, data } = useQuery(COINS);
    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error :(</p>;

    return data.coins.map(
      ({ id, symbol, name }: { id: number; symbol: string; name: string }) => (
        <div key={symbol}>
          <Wrap>
            <Box
              bg="dark"
              w="100%"
              p={3}
              color="white"
              borderWidth="1px"
              borderRadius="lg"
              display="flex"
              mt="2"
              alignItems="center"
            >
              <p>
                {symbol}: {name}
              </p>
              <Spacer />
              <Box>
                <Button key={id} colorScheme="blue" mt="2" ml="2">
                  Info
                </Button>
                <Button colorScheme="green" mt="2" ml="2">
                  Hide
                </Button>
              </Box>
            </Box>
          </Wrap>
        </div>
      )
    );
  };

  return (
    <div>
      <Heading p={7} >CryptoPeek</Heading>
      <Spacer />
      {DisplayCoins()}
    </div>
  );
};

export default CoinList;
