import React from "react";
import { Box, Spacer, Badge } from "@chakra-ui/react";
import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  useQuery,
  gql,
} from "@apollo/client";

const CoinInfo = () => {
  const SINGLECOIN = gql`
    query GetCoinInfo {
      coins {
        id
        symbol
        name
        price_usd
        rank
        price_btc
        percent_change_24h
        market_cap_usd
      }
    }
  `;

  const CoinStats = () => {
    const { loading, error, data } = useQuery(SINGLECOIN);
    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error :(</p>;

    return data.coins[1];
  };

  const InfoCard = () => {
    const Coin = {
      rank: CoinStats().rank,
      symbol: CoinStats().symbol,
      usd: CoinStats().price_usd,
      btc: CoinStats().price_btc,
      name: CoinStats().name,
      h24Change: CoinStats().percent_change_24h,
      marketCap: CoinStats().market_cap_usd,
    };

    return (
      <Box
        maxW="sm"
        borderWidth="1px"
        borderRadius="lg"
        overflow="hidden"
        p={3}
        m={7}
      >
        <Box p="6">
          <Box display="flex" alignItems="baseline">
            <Badge borderRadius="full" px="2" colorScheme="teal">
              {Coin.symbol}
            </Badge>
            <Box
              color="gray.500"
              fontWeight="semibold"
              letterSpacing="wide"
              fontSize="xs"
              textTransform="uppercase"
              ml="2"
            >
              USD:{Coin.usd} &bull; BTC{Coin.btc} 
            </Box>
          </Box>

          <Box
            mt="1"
            fontWeight="semibold"
            as="h4"
            lineHeight="tight"
            isTruncated
          >
            {Coin.name}
          </Box>

          <Box>
            {Coin.h24Change}
            <Box as="span" color="gray.600" fontSize="sm">
              / 24h Change
            </Box>
          </Box>

          <Box as="span" ml="2" color="gray.600" fontSize="sm">
          Market Cap: {Coin.marketCap} 
          </Box>
        </Box>
      </Box>
    );
  };

  return <div>{InfoCard()}</div>;
};

export default CoinInfo;
