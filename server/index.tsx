const { ApolloServer, gql } = require('apollo-server');
const axios = require('axios');

// A schema is a collection of type definitions (hence "typeDefs")
// that together define the "shape" of queries that are executed against
// your data.
const typeDefs = gql`
  # Comments in GraphQL strings (such as this one) start with the hash (#) symbol.

  # This "Book" type defines the queryable fields for every book in our data source.
  type Coin {
    id: String
    symbol: String
    name: String
    nameid: String
    rank: Int
    price_usd: String
    percent_change_24h: String
    percent_change_1h: String
    percent_change_7d: String
    price_btc: String
    market_cap_usd: String
    volume24: Float
    volume24a: Float
    csupply: String
    tsupply: String
    msupply: String
  }

  # The "Query" type is special: it lists all of the available queries that
  # clients can execute, along with the return type for each. In this
  # case, the "books" query returns an array of zero or more Coins (defined above).
  type Query {
    coins: [Coin]
  }
`;


const coins = async ()=> {
  const res = await axios.get("https://api.coinlore.net/api/tickers/");
  return res.data.data;
};


// Resolvers define the technique for fetching the types defined in the
// schema. This resolver retrieves books from the "books" array above.
const resolvers = {
    Query: {
      coins: () => coins(),
    },
  };


// The ApolloServer constructor requires two parameters: your schema
// definition and your set of resolvers.
const server = new ApolloServer({ typeDefs, resolvers });

// The `listen` method launches a web server.
server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});

