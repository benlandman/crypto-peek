# README #

This readme details the full setup and start instructions in order to run the project.

### FEATURES ###

* Linked directly to a graphQL apollo Server
* Pulls live data from the back-end server to the front-end react client
* Displays list of all coins

### SETUP INSTRUCTIONS? ###

*  1.Use the following to clone the repository(the finished project is on the master branch):
   - git clone git@bitbucket.org:benlandman/crypto-peek.git
* 2.SERVER SETUP
    - Install the following server dependancies by running these commands:
        - cd server/
        - npm init --yes
        - npm install apollo-server graphql
        - npm install axios
    - Now start the server by running: node index.tsx
* 3.CLIENT SETUP
    - Install the following client dependancies by running these commands:
        - cd client/
        - npm install @apollo/client graphql
        - npm i @chakra-ui/react @emotion/react@^11 @emotion/styled@^11 framer-motion@^6
    - Now start the client by running: npm start
* 4.You can access the website by navigating @ http://localhost:3000/ in your chrome browser
* 5.You can also access the graphQL explorer @ http://localhost:4000/ in your chrome browser



### Contributors ###

* Julian Mackay  - Front-End contrtibutor
* Ben Landman - Back-End contributor


